# Comet

## About Us
Comet is an executor designed to be a sideproject focused on the experimenting on the business side of ROBLOX Executors. 

Comet itself is a great executor, with a design that is full of color and differenciates from competitors. Paired with a DLL that is powerful and works on many games.

## What this is used for?
This is used for online connectivity operations like updating and hosting things like text files and json files. 
